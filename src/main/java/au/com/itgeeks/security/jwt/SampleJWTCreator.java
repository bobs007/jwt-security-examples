package au.com.itgeeks.security.jwt;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;


/**
 * It creates private and public key.
 * 
 * JWT by using private key and JWT will be verified by using public key.
 * 
 *    used library  io.jsonwebtoken.*    
 * 
 * Credits to these links.
 *  https://stackoverflow.com/questions/37722090/java-jwt-with-public-private-keys
 *  
 * 
 * @author bobs
 *
 */
public class SampleJWTCreator {

  private final static Logger logger = LoggerFactory.getLogger(SampleJWTCreator.class);

  public static void main(String[] args) {

    logger.debug("-- Generating Keys --");
    KeyPair rsaKeys = null;

    try {
      rsaKeys = getRSAKeys();
    } catch (Exception e) {
      logger.error("Exception thrown {}", e);
    }
    PublicKey publicKey = rsaKeys.getPublic();
    PrivateKey privateKey = rsaKeys.getPrivate();

    logger.debug("-- generated keys --");
    
    printKeys(rsaKeys);

    String token = generateToken(privateKey);

    logger.debug("Generated Token: {}", token);

    verifyToken(token, publicKey);
    
  }

  /*
   * Prints keys in two different format.
   *  
   * @param pair
   */
  private static void printKeys(KeyPair pair) {
    String encodedPrivateKey = Base64.getEncoder().encodeToString(pair.getPrivate().getEncoded());
    String encodedPublicKey = Base64.getEncoder().encodeToString(pair.getPublic().getEncoded());

    logger.debug("-----BEGIN RSA PRIVATE KEY-----");
    logger.debug(encodedPrivateKey);
    logger.debug("-----END RSA PRIVATE KEY-----");
    logger.debug("-----BEGIN RSA PUBLIC KEY-----");
    logger.debug(encodedPublicKey);
    logger.debug("-----END RSA PUBLIC KEY-----");
    String encodedPrivateKey1 =
        Base64.getMimeEncoder().encodeToString(pair.getPrivate().getEncoded());
    String encodedPublicKey1 =
        Base64.getMimeEncoder().encodeToString(pair.getPublic().getEncoded());
    logger.debug("-----BEGIN RSA PRIVATE KEY-----");
    logger.debug(encodedPrivateKey1);
    logger.debug("-----END RSA PRIVATE KEY-----");
    logger.debug("-----BEGIN RSA PUBLIC KEY-----");
    logger.debug(encodedPublicKey1);
    logger.debug("-----END RSA PUBLIC KEY-----");
  }

  /*
   * Generating the keys by using private key
   *     
   * @param privateKey
   * @return
   */
  private static String generateToken(PrivateKey privateKey) {
    String token = null;
    try {
      Map<String, Object> claims = new HashMap<String, Object>();

      claims.put("id", "xxx");
      claims.put("role", "user");
      claims.put("created", new Date());

      token =
          Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.RS512, privateKey).compact();


    } catch (Exception e) {
      logger.error("exception {}", e);
    }
    return token;
  }

  /*
   * Verify the token by using public key  
   *   
   * @param token
   * @param publicKey
   * @return
   */
  private static Claims verifyToken(String token, PublicKey publicKey) {
    Claims claims;
    try {
      claims = Jwts.parser().setSigningKey(publicKey).parseClaimsJws(token).getBody();

      logger.debug("ID {}", claims.get("id").toString());
      logger.debug("Role {}", claims.get("role").toString());

    } catch (Exception e) {
      claims = null;
    }
    return claims;
  }
  
  /*
   *  Get RSA keys. Uses key size of 2048.
   */
  private static KeyPair getRSAKeys() throws Exception {
    KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
    keyPairGenerator.initialize(2048);
    KeyPair keyPair = keyPairGenerator.generateKeyPair();
    PrivateKey privateKey = keyPair.getPrivate();
    PublicKey publicKey = keyPair.getPublic();
    KeyPair keypair = new KeyPair(publicKey, privateKey);
    return keypair;
  }

}