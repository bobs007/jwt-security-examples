package au.com.itgeeks.security.jwt;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;
import java.util.Base64;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSObject;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.Payload;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jwt.JWT;


/**
 * 
 * https://connect2id.com/products/nimbus-jose-jwt/examples/jws-with-rsa-signature
 * 
 * @author bobs
 *
 */
public class SampleJWTCreator1 {
  
  private final static Logger logger = LoggerFactory.getLogger(SampleJWTCreator1.class);

  public static void main(String[] args) throws Exception {
  
    logger.debug("Nimbus JWT using library ");
    
    KeyPair rsaKeys = null;

    try {
      rsaKeys = getRSAKeys();
    } catch (Exception e) {
      logger.error("Exception thrown {}", e);
    }

    logger.debug("-- generated keys --");
    
    printKeys(rsaKeys);
    
    String jwt = createJWT(rsaKeys);
    
    verifyJWT(jwt, rsaKeys);
    
    JWK jwk = createJWK(rsaKeys);
  
    verifyByUsingJWK(jwt, jwk);
    
    logger.debug("Nimbus JWT using library ");
    
  }
  
  private static void verifyByUsingJWK(String jwt, JWK jwk) throws ParseException, JOSEException {
    
    JWSObject jwsObject = JWSObject.parse(jwt);

    JWSVerifier verifier = new RSASSAVerifier((RSAKey)jwk);

    logger.debug("verification result => {}" , jwsObject.verify(verifier));

    logger.debug("payload retrived => {}" , jwsObject.getPayload().toString());
    
  }
  
  /*
   * 
   * @param pair
   */
  private static JWK createJWK(KeyPair pair) {
    // Convert to JWK format
    JWK jwk = new RSAKey.Builder((RSAPublicKey)pair.getPublic())
        .privateKey((RSAPrivateKey)pair.getPrivate())
        .keyID(UUID.randomUUID().toString()) // Give the key some ID (optional)
        .build();

    // Output
    logger.debug(jwk.toJSONString());
    
    JWK jwk1 = new RSAKey.Builder((RSAPublicKey)pair.getPublic())
        .keyID(UUID.randomUUID().toString()) // Give the key some ID (optional)
        .build();

    // Output
    logger.debug(jwk1.toJSONString());
    
    return jwk1;
  }
  
  /*
   * Prints keys in two different format.
   *  
   * @param pair
   */
  private static void printKeys(KeyPair pair) {
    String encodedPrivateKey = Base64.getEncoder().encodeToString(pair.getPrivate().getEncoded());
    String encodedPublicKey = Base64.getEncoder().encodeToString(pair.getPublic().getEncoded());

    logger.debug("-----BEGIN RSA PRIVATE KEY-----");
    logger.debug(encodedPrivateKey);
    logger.debug("-----END RSA PRIVATE KEY-----");
    logger.debug("-----BEGIN RSA PUBLIC KEY-----");
    logger.debug(encodedPublicKey);
    logger.debug("-----END RSA PUBLIC KEY-----");
    String encodedPrivateKey1 =
        Base64.getMimeEncoder().encodeToString(pair.getPrivate().getEncoded());
    String encodedPublicKey1 =
        Base64.getMimeEncoder().encodeToString(pair.getPublic().getEncoded());
    logger.debug("-----BEGIN RSA PRIVATE KEY-----");
    logger.debug(encodedPrivateKey1);
    logger.debug("-----END RSA PRIVATE KEY-----");
    logger.debug("-----BEGIN RSA PUBLIC KEY-----");
    logger.debug(encodedPublicKey1);
    logger.debug("-----END RSA PUBLIC KEY-----");
  }

  /*
   *  Get RSA keys. Uses key size of 2048.
   */
  private static KeyPair getRSAKeys() throws Exception {
    KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
    keyPairGenerator.initialize(2048);
    KeyPair keyPair = keyPairGenerator.generateKeyPair();
    PrivateKey privateKey = keyPair.getPrivate();
    PublicKey publicKey = keyPair.getPublic();
    KeyPair keypair = new KeyPair(publicKey, privateKey);
    return keypair;
  }
  
  private static String createJWT(KeyPair pair) throws JOSEException {
    // Create RSA-signer with the private key
    JWSSigner signer = new RSASSASigner(pair.getPrivate());

    // Prepare JWS object with simple string as payload
    JWSObject jwsObject = new JWSObject(
        new JWSHeader.Builder(JWSAlgorithm.RS256).keyID("123").build(), 
        new Payload("In RSA we trust!"));

    // Compute the RSA signature
    jwsObject.sign(signer);
    
    logger.debug(jwsObject.serialize());
    
    return jwsObject.serialize();
  }
  
  private static void verifyJWT(String jwt, KeyPair pair) throws JOSEException, ParseException {
    JWSObject jwsObject = JWSObject.parse(jwt);

    JWSVerifier verifier = new RSASSAVerifier((RSAPublicKey) pair.getPublic());

    logger.debug("verification result => {}" , jwsObject.verify(verifier));

    logger.debug("payload retrived => {}" , jwsObject.getPayload().toString());
  }
  

}